//
// Created by Gabriel Augendre on 28/04/2021.
//

#ifndef FASTEST_PRESSER_MAIN_H
#define FASTEST_PRESSER_MAIN_H

void displayCountdown();
void displayDigit(const char *digit);
byte computeWinner();
byte isCheating();
void lcdSecondLine();
void printScore();
void waitForReady();

#endif //FASTEST_PRESSER_MAIN_H
