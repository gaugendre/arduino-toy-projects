//
// Created by Gabriel Augendre on 29/04/2021.
//

#ifndef SUPER_SIMON_MAIN_H
#define SUPER_SIMON_MAIN_H

#define LCD_COLS 16
#define LCD_ROWS 2

#define BUZZER 10

#define BUTTON_GREEN 2
#define BUTTON_YELLOW 11
#define BUTTON_BLUE 12
#define BUTTON_RED 3

#define LED_GREEN A5
#define LED_YELLOW A4
#define LED_BLUE A3
#define LED_RED A2

#define TONE_GREEN 165
#define TONE_YELLOW 277
#define TONE_BLUE 329
#define TONE_RED 440
#define TONE_ERROR 100

#define GREEN_INDEX 0

#define MAX_GAME 20

#endif //SUPER_SIMON_MAIN_H
