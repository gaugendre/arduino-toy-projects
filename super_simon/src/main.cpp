#include <Arduino.h>
#include <LiquidCrystal.h>
#include "main.h"
#include "utils.h"


LiquidCrystal lcd(9, 8, 4, 5, 6, 7);
byte sequence[MAX_GAME] = {};
int8_t currentPosition = -1;
bool win = true;
bool over = false;

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    lcd.begin(LCD_COLS, LCD_ROWS);
    lcd.print("Super Simon");
    randomSeed(analogRead(A0));
    pinMode(BUZZER, OUTPUT);
    configure();
    deactivateAll();
    digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
    if (currentPosition + 1 >= MAX_GAME) {
        over = true;
    }
    if (over) {
        endGame(lcd, win, currentPosition);
        return;
    }

    delay(1200);

    currentPosition += 1;
    byte newItem = random(0, 4);
    sequence[currentPosition] = newItem;

    playSequence(sequence, currentPosition);
    if (!userSequence(sequence, currentPosition)) {
        win = false;
        over = true;
    }
}
